Interface with a StorkAir WHR9XX on Domoticz

Publish every few seconds the status on a MQTT Domoticz/in topic
Listen on MQTT topic for commands to set the ventilation level

todo :
- set bypass temperature
- check on faulty messages
- serial check


The following packages are needed:

- sudo apt-get install python3-serial python3-pip python3-yaml

- sudo pip3 install paho-mqtt


Add the following dummy Devices in Domoticz :

- OutsideAirTemp        # temperature

- SupplyAirTemp         # temperature

- ReturnAirTemp         # temperature

- ExhaustAirTemp        # temperature

- IntakeFanActive       # text

- FanLevel              # text

- Filter                # text

- Selector              # selector switch 0=auto 10=away 20=low 30=middle 40=high !! Depends on model/CO2 sensor etc !!

- SelectorDisplay       # selector switch 0=manual 10=away 20=low 30=middle 40=high !! This selector is needed only if
                        # you steer the fan level from a script because you have a CO2 sensor or a schedule.
                        # This switch has "protected" = True, in order to behave like a display only item


After installing the dependencies, clone this repository and modify the settings in src/whr930.py and run sudo make install.

$ git clone https://gitlab.com/matthijs_rademakers/StorkAir-Zehnder-WHR-930-Domoticz-MQTT.git


edit src/whr930.py and fill the IDX's, serialport and MQTTserver address.

- SerialPort='/dev/YPort'       # Serial port WHR930
- MQTTServer='127.0.0.1'        # IP MQTT broker

$ sudo make install

To test if everything is working fine :

python3 /usr/local/bin/whr930.py

To start the whr930 as service :

$ sudo systemctl start whr930.service
